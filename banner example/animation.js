/*

    ------------------------------------
    ----------- Variables --------------
    ------------------------------------
    
*/

var tl     = new TimelineMax();
var tw     = TweenMax;
var banner = {};

banner.id               = document.querySelector('.canvas');
banner.clickThrough     = document.querySelector('.click-through');


/*

    ------------------------------------
    ----------- preparation ------------
    ------------------------------------
    
*/


// init
banner.init = function() {
    this.eventHandler = {
        
        click: {
            exit : this.exit.bind(this),
        },

        mouse: {
            setEvents       : this.setMouseEvents.bind(this),
            over            : this.mouseOver.bind(this),
            out             : this.mouseOut.bind(this),
        },

        animation: {
            init    : this.init.bind(this),
            reset   : this.reset.bind(this),
            play    : this.play.bind(this),
        }
    }
    
    this.clickThrough.addEventListener('click', this.eventHandler.click.exit, false);
    this.eventHandler.animation.reset();
}


/*

    ------------------------------------
    ------------- Events ---------------
    ------------------------------------
    
*/


// Exit
banner.exit = function() {
    window.open(window.clickTag);
}

// Set Mouse Events
banner.setMouseEvents = function() {
    this.id.addEventListener('mouseover', this.eventHandler.mouse.over, false);
    this.id.addEventListener('mouseout', this.eventHandler.mouse.out, false);
}

// Mouse Over
banner.mouseOver = function() {
    tw  .to('.cta', 0.5, { x:4, ease: Power2.easeOut })
}

// Mouse Out
banner.mouseOut = function() {
    tw  .to('.cta', 0.5, { x:0, ease: Power2.easeOut })
}


/*

    ------------------------------------
    ------------ Animation -------------
    ------------------------------------
    
*/


// reset
banner.reset = function() {
    /* --- do-not-edit --- */
    tw  .set(["div", "img"], { force3D: true, backfaceVisibility: "hidden", rotationZ: '0.01deg', z:0.01 });
	tw	.set(".overlay", 	 { autoAlpha: 1.0 } );
    tw	.set(".canvas",  { visibility: 'visible' });
    /* --- do-not-edit --- */ 
    
	tw	.set(".background",  { y: "100%" });
	tw	.set(".title", 		 { x: -20, autoAlpha: 0.0 });
	tw	.set(".sub-title",   { x: -20, autoAlpha: 0.0 });
	tw	.set(".cta", 		 { x: -20, autoAlpha: 0.0 });
 
    this.eventHandler.animation.play();
}

// Play
banner.play = function() {
    
    tl	.addLabel("overlay", "+=0.1")
		.to(".overlay", 		0.5, { delay: 0.0, autoAlpha:0.0, ease:Power2.easeOut }, "overlay")
	
	tl	.addLabel("Frame01", "-=0.5")
		.to(".background",      1.5, { delay: 0.0, y:'0%', autoAlpha: 1.0, ease:Power2.easeInOut} , "Frame01")
		.to(".title",           0.5, { delay: 0.5, x:0, autoAlpha: 1.0, ease:Power2.easeInOut} , "Frame01")
		.to(".sub-title",       0.5, { delay: 1.0, x:0, autoAlpha: 1.0, ease:Power2.easeInOut} , "Frame01")
	
	tl	.addLabel("Frame02", "+=0.5")
		.to(".cta", 			1.0, {delay: 0.0, x:0, autoAlpha: 1.0, ease:Power2.easeOut, onStart: this.eventHandler.mouse.setEvents }, "Frame02")
}

